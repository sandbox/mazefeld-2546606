<?php

/**
 * Helper function
 *
 * @return array
 */
function media_organizer_get_module_path() {

  return array(
    'media_organizer' => drupal_get_path('module', 'media_organizer'),
    'media' => drupal_get_path('module', 'media')
  );
}

/**
 * Admin page for managing and organize media files
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function media_organizer_admin($form, &$form_state) {

  $module_path =  media_organizer_get_module_path();

  // Include external library
  include_once $module_path['media'] . '/includes/media.admin.inc';

  $form = array();

  // Build form
  drupal_add_library('system', 'ui.draggable');
  drupal_add_library('system', 'ui.droppable');

  // Set js settings
  $setting = array(
    'media' => array(
      'organizer' => array(
        'addTagUrl' => url(MEDIA_ORGANIZER_PATH_ADD_TAG),
        'removeTagUrl' => url(MEDIA_ORGANIZER_PATH_REMOVE_TAG),
        'loadFiles' => url(MEDIA_ORGANIZER_LIST),
        'limit' => 9
      )));

  drupal_add_js($setting, 'setting');

  // Set js settings
  $setting = array(
    'media' => array(
      'browser' => array(
        'library' => array(
          'getMediaUrl' => url(MEDIA_ORGANIZER_LIST)
        )
      )));

  drupal_add_js($setting, 'setting');

  $form['#attached']['js'] = array('system', 'ui.draggable');

  $form['#attached'] = array(
    'js' => array(
      $module_path['media'] . '/js/media.core.js',
      $module_path['media'] . '/js/plugins/media.library.js',
      $module_path['media_organizer'] . '/js/media_organizer.js'
    ),
    'css' => array($module_path['media_organizer'] . '/css/media_organizer.css'),
  );


  $form['basic'] = array (
    '#markup' => '<h2>Media organizer</h2>',
  );

  $form['#prefix'] = '<div id="media-organizer" class="media-organizer-browser">';
  $form['#suffix'] = '</div>';

  // Add tag area
  $form['tag_area'] = _media_organizer_view_tag_area();

  // Add list
  $form['list'] = _media_organizer_media_list($form);

  return $form;
}

/**
 * Returns render array for tag area
 *
 * @return array render array
 */
function _media_organizer_view_tag_area() {

  // Load taxonomy
  $vocabulary = taxonomy_vocabulary_machine_name_load(MEDIA_ORGANIZER_TAGS_MACHINE_NAME);
  $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));

  $tag_area = array();

  $tag_area['#prefix'] = '<div id="media-organizer-tag-area" class="tag-area">';
  $tag_area['#suffix'] = '</div>';

  foreach($terms as $term) {
    $tag_area[] = array(
      '#prefix' => '<div data-term-id="'. $term->tid .'" class="term-drop-area">',
      '#markup' => $term->name,
      '#suffix' => '</div>'
    );
  }
  return $tag_area;
}


/**
 * Generates list container markup
 *
 * @param $parent_form
 * @return mixed render array (form) for list wrapper
 */
function _media_organizer_media_list(&$parent_form) {

  $form['files'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div class="media-display-thumbnails" id="scrollbox"><ul id="media-browser-library-list" class="media-list-thumbnails">',
    '#suffix' => '</ul></div>',
  );

  return $form;
}

/**
 * Selects images from db and returns json
 *
 * - Accepts filter parameter by url
 *
 * @param $parent_form
 */
function media_organizer_load_files(&$parent_form) {

  // Load necessary libraries
  module_load_include('inc', 'media', 'includes/media.browser');

  // Set variables
  $params = drupal_get_query_parameters();
  $start = isset($params['start']) ? $params['start'] : 0;
  $limit = isset($params['limit']) ? $params['limit'] : media_variable_get('browser_pager_limit');
  $limit = 9;
  $tag_filter = (strlen($params['tag_filter'])) ? explode(',', filter_xss($params['tag_filter'])) : FALSE;

  // Build query
  $query = db_select('file_managed', 'f');
  $query =
  $query->fields('f', array('fid'));
  $query->range($start, $limit);
  $query->orderBy('f.timestamp', 'DESC');

  // Filter by tag id
  if($tag_filter) {
    $query->join('field_data_field_media_organizer_tags', 'mot', 'f.fid = mot.entity_id' );
    $query->condition('mot.field_media_organizer_tags_tid', $tag_filter, 'IN');
  }

  // Filter by mimetype image
  $query->condition('f.filemime', '%image%', 'LIKE');

  // @todo Implement granular editorial access: http://drupal.org/node/696970.
  //   In the meantime, protect information about private files from being
  //   discovered by unprivileged users. See also media_view_page().
  if (!user_access('administer media')) {
    $query->condition('f.uri', db_like('private://') . '%', 'NOT LIKE');
  }

  $query->condition('f.status', FILE_STATUS_PERMANENT);

  // Fetch all images by id
  $fids = $query->execute()->fetchCol();
  $files = file_load_multiple($fids);
  foreach ($files as $file) {
    media_organizer_build_media_item($file);
  }

  drupal_json_output(array('media' => array_values($files)));
  exit();
}


/**
 * Adds properties to the passed in file that are needed by the media browser JS code.
 */
function media_organizer_build_media_item($file) {
  $preview = media_organizer_get_thumbnail($file, FALSE);
  $file->preview = drupal_render($preview);
  $file->url = file_create_url($file->uri);
}

/**
 * Build render array for thumbnails
 *
 * - Setting customized theme wrapper
 *
 * @param $file
 * @param null $link
 * @return array|null
 * @throws \Exception
 */
function media_organizer_get_thumbnail($file, $link = NULL) {

  // If a file has an invalid type, allow file_view_file() to work.
  if (!file_info_file_types($file->type)) {
    $file->type = media_get_type($file);
  }

  $preview = file_view_file($file, 'media_preview');
  $preview['#show_names'] = TRUE;
  $preview['#add_link'] = $link;
  $preview['#theme_wrappers'][] = 'media_organizer_thumbnail';

  return $preview;
}

/**
 * Customized theme wrapper for thumbnails
 *
 * @param $variables
 * @return string rendered thumbnail markup
 */
function theme_media_organizer_thumbnail($variables) {

  // Set vars
  $element = $variables['element'];
  $destination = drupal_get_destination();
  $tags = array();
  $file = $element['#file'];

  // Get tags from image
  if(isset($file->field_media_organizer_tags['und'])) {
    $tags = $file->field_media_organizer_tags['und'];
  }

  // Arguments for the thumbnail link
  $thumb = $element['#children'];
  $target = 'media/' . $element['#file']->fid . '/edit';
  $options = array(
    'query' => $destination,
    'html' => TRUE,
    'attributes' => array(
      'title' => t('Click to edit details'),
      'data-fid' => $element['#file']->fid
    )
  );

  // Wrappers to go around the thumbnail
  $output['#prefix'] = '<div class="media-thumbnail" data-fid="' . $element['#file']->fid . '">';
  $output['#suffix'] = '</div>';

  // Add image markup
  if (!empty($element['#add_link'])) {
    $inner_img = l($thumb, $target, $options);
  }
  else {
    $inner_img = $thumb;
  }

  // Img Container
  $output['img'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => 'img-wrapper',
      'data-fid' => $element['#file']->fid
    ),
    'inner' => array('#markup' => $inner_img),

  );

  // Meta Container
  $output['meta'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => 'meta-wrapper'
    )
  );

  $output['meta']['filename'] = array (
    '#type' => 'container',
    '#attributes' => array(
      'class' => 'meta-filename meta-info'
    ),
    'inner' => array('#markup' => $file->filename)
  );

  // Filesize
  $output['meta']['size'] = array (
    '#type' => 'container',
    '#attributes' => array(
      'class' => 'meta-filesize meta-info'
    ),
    'inner' => array('#markup' => format_size($file->filesize))
  );

     // Timestamp
  $output['meta']['size'] = array (
    '#type' => 'container',
    '#attributes' => array(
      'class' => 'meta-filesize meta-info'
    ),
    'inner' => array('#markup' => $file->timestamp)
  );

  // Get terms
  $term_ids = array();
  foreach($tags as $tag) {
    $term_ids[] = $tag['tid'];
  }

  $terms = taxonomy_term_load_multiple($term_ids);

  // Add tag list array
  $output['meta'][] = _media_organizer_tag_bar($terms, $file);



  return render($output);
}

/**
 * Create render array for tag-bar for each image
 *
 * @param $terms array of
 * @param $file object image files
 * @return array render array with markup
 */
function _media_organizer_tag_bar($terms, $file) {

  $output = array();

  // Add wrapper
  $output['#prefix'] = '<div class="img-tags">';
  $output['#suffix'] = '</div>';


  // Add markup for each tag
  foreach($terms as $term) {

    $tag_markup = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'file_tag_' . $file->fid . '_' . $term->tid,
        'class' => 'img-tag',
        'data-tid' => $term->tid
      ),
      'inner' => array(
        '#markup' => $term->name
      ),
      'link' => array(
        '#markup' => l('X', '#', array(
          'attributes' => array(
            'class' => 'tag-btn delete-action',
            'data-tid' => $term->tid,
            'data-fid' => $file->fid
          ),
        ))
      )
    );


    $output[] = $tag_markup;
  }

  return $output;
}

/**
 * Add taxonomy reference to file entity
 *
 * - add media tag
 * - fetches arguments by url
 */
function media_organizer_add_tag() {

  // Get the URL params
  $term_id = (integer) urldecode($_GET['term_id']);
  $file_id = (integer) urldecode($_GET['file_id']);

  $file = file_load($file_id);

  // Check if relation already exists
  foreach($file->field_media_organizer_tags['und'] as $term_reference) {
    if($term_reference['tid'] == $term_id) {

      // Quit if term reference already exists
      return;
    }
  }

  // Add Term reference and save
  $file->field_media_organizer_tags['und'][] = array(
    'tid' => $term_id
  );

  file_save($file);
}


/**
 * Removes taxonomy tags from given file
 */
function media_organizer_remove_tag() {

  // Get the URL params
  $term_id = (integer) urldecode($_GET['term_id']);
  $file_id = (integer) urldecode($_GET['file_id']);

  $file = file_load($file_id);

  // Check if relation already exists
  foreach($file->field_media_organizer_tags['und']  as $key => $term_reference) {
    if($term_reference['tid'] == $term_id) {

      // Quit if term reference already exists

      unset($file->field_media_organizer_tags['und'][$key]);
      file_save($file);
    }
  }
}