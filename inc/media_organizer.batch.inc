<?php

function media_organizer_batch_form($form, &$form_state) {

  $form['check'] = array(
    '#type' => 'textfield',
    '#title' => 'Process all files'
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

function media_organizer_batch_form_submit($form, &$form_state) {

  drupal_set_message('Media organizer batch startet');

  $operations_per_request = 5;
  $file_count = db_query('SELECT COUNT(DISTINCT fid) FROM {file_managed}')->fetchField();

  $num_operations = (integer) ceil($file_count / $operations_per_request);

  $batch = array(
    'finished' => 'media_organizer_batch_finished',
    'title' => t('Processing Media Organizer Batch'),
    'init_message' => t('Media Organizer Batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Media Organizer Batch has encountered an error.'),
    'file' => drupal_get_path('module', 'media_organizer') . '/inc/media_organizer.batch.inc',
  );

  for ($i = 0; $i < $num_operations; $i++) {

    // Each operation is an array consisting of
    // - The function to call.
    // - An array of arguments to that function.
    $operations[] = array(
      'media_organizer_files_process',
      array(
        $i * $operations_per_request, // current
        $file_count, // total
        $operations_per_request, // per request
      ),
    );
  }

  $batch['operations'] = $operations;

  batch_set($batch);
}


/**
 * Batch Operation Callback
 *
 */
function media_organizer_files_process($current, $total, $per_request, &$context) {

  $start = $current;
  $limit = $per_request;

  $query = db_select('file_managed', 'f');
  $query
    ->fields('f', array('fid'))
    ->range($start, $limit)
    ->orderBy('f.timestamp', 'DESC');

  // Fetch all images by id
  $fids = $query->execute()->fetchCol();
  $files = file_load_multiple($fids);


  foreach ($files as $file) {
    $current++;

    media_organizer_set_file_path($file);
  }

  usleep( 400000 );
  $context['message'] = t('Process %current of %total', array('%current' => $current, '%total' => $total));
}

function media_organizer_set_file_path($file) {

  // TODO: Improve performance
  $term = taxonomy_term_load(media_organizer_get_primary_tid($file));

  if($term) {
    $term_folder =  media_organizer_sanitize_tag($term->name);
  }
  else {
    $term_folder = variable_get('media_organizer_no_tag_folder', MEDIA_ORGANIZER_NO_TAG_FOLDER);
  }


  $root_folder = variable_get('media_organizer_default_folder', MEDIA_ORGANIZER_DEFAULT_FOLDER);

  $new_directory = 'public://' . $root_folder . '/' . $term_folder;

  $new_file_path = $new_directory . '/' . $file->filename;

  if(file_prepare_directory($new_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
    file_move($file, $new_file_path);
  }
  else {
    $message = t("Folder %folder is not writable or doesn't exists", array('%folder' => $new_directory));

    watchdog('media_organizer', $message);

    drupal_set_message($message);
  }
}

function media_organizer_sanitize_tag($string) {

  // Replaces all spaces with underscore
  $string = str_replace(' ', '_', $string);

  // Removes special chars.
  $string =  preg_replace('/[^A-Za-z0-9\_]/', '', $string);

  return strtolower($string);
}

/**
 * Returns the primary taxonomy (tags) id of a file object
 *
 * @param $file obj
 *   File object with related tags field
 *
 * @return bool mixed
 *   Return false or the primary taxonomy id
 *
 */
function media_organizer_get_primary_tid($file) {
  if(isset($file->field_media_organizer_tags['und'][0]['tid'])) {
    return $file->field_media_organizer_tags['und'][0]['tid'];
  }

  return false;
}

/**
 * Batch 'finished' callback
 */
function media_organizer_batch_finished($success, $results, $operations) {
  if ($success) {
// Here we do something meaningful with the results.
    $message = t('@count items successfully processed:', array('@count' => count($results)));
// $message .= theme('item_list', $results);  // D6 syntax
    $message .= theme('item_list', array('items' => $results));
    drupal_set_message($message);
  }
  else {
// An error occurred.
// $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}
