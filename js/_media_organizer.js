(function ($) {
  namespace('Drupal.media');

  Drupal.behaviors.organizer = {
    attach: function (context, settings) {

      var library = new Drupal.media.organizer(Drupal.settings.media.organizer)

      // Init static concent
      library.initDom();

      // Init dynamic content
//      library.refreshDom();

      // Initially load thumbnails
      library.loadFiles();

      // Init dynamic content
//      library.refreshDom();

      $('#media-organizer-list').bind('scroll', library, library.scrollUpdater);
    }
  };

  /**
   * Library constructer
   * - Set defaults
   *
   * @param settings
   */
  Drupal.media.organizer = function (settings) {
    this.settings = {};

    this.renderElement = $('.media-display-thumbnails');

    this.done = false; // Keeps track of if the last request for media returned 0 results.

    this.cursor = 0; // keeps track of what the last requested media object was.
    this.mediaFiles = []; // An array of loaded media files from the server.
    this.selectedMediaFiles = [];

    this.filter = {};

    this.filter.position = 0;

    this.loading = false;

    $.extend(this.settings, settings);

//    console.log('Set settings:',  this.settings);
  }

  /**
   * Init dom and attach event listener
   * - adding droppable event to tag area
   * - adding click event to tag filter
   */
  Drupal.media.organizer.prototype.initDom = function () {
    var that = this;

    // Set selectors
    var droppable_selector = $('#media-organizer .term-drop-area');

    // Init droppable event
    droppable_selector.droppable({
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      drop: function( event, ui ) {
//        console.log('Dropped item', term_id, file_id);

        // Get attributes
        var drop_container = $( this );
        var drag_element =  $(ui.draggable[0]);
        var term_id = drop_container.attr('data-term-id');
        var file_id = drag_element.attr('data-fid');

        that.addTag(term_id, file_id);
      }
    });

    // Add filter events to tags
    droppable_selector.click(function (e){

      // Toggle active class on clicked
      $(this).toggleClass('ui-state-active');

      // TODO: Speedup selector
      var attr_list = [];
      $('#media-organizer .term-drop-area.ui-state-active').each(function() {
        attr_list.push($(this).attr('data-term-id'));
      });

//      // Set global filter
      that.filter.tag_filter = attr_list.join(',');


      that.loadFiles();
    });
  }

  /**
   * Set event listener to new objects in dom
   * - draggable event
   * - remove tag event
   */
  Drupal.media.organizer.prototype.refreshDom = function () {
    var that = this;

    // Set selectors
    var draggable_selector = $('#media-organizer .media-thumbnail .img-wrapper');
    var delete_tag_selector = $('#media-organizer .delete-action');

    console.log('Media organizer: Refresh dom ...');

    // Attach remove tag event
    delete_tag_selector.click(function (e, s) {
      event.preventDefault();

      var action_link = $(this);
      var term_id = action_link.attr('data-tid');
      var file_id = action_link.attr('data-fid');

      that.removeTag(term_id, file_id);
    });

    // Init draggable event
    draggable_selector.draggable(
      {
        appendTo: 'body',
//        revert : true,
        helper: 'clone'
      });
  }

  /**
   * Request files via ajax and call render function
   */
  Drupal.media.organizer.prototype.loadFiles = function () {
    var that = this;

    var gotMedia = function (data, status) {
      console.log('CALL SUCCESS: loadFiles', data, status);

      that.mediaFiles = that.mediaFiles.concat(data.media);
      that.render(that.renderElement);

//      that.filter.cursor += data.length;

      // Remove the flag that prevents loading of more media
      that.loading = false;
    };

    var errorCallback = function () {
      console.log(Drupal.t('Error getting media.'));
    };

    $.ajax({
      url: this.settings.loadFiles,
      type: 'GET',
      dataType: 'json',
      data: that.filter,
      error: errorCallback,
      success: gotMedia
    });
  }

  Drupal.media.organizer.prototype.addTag = function (term_id, tag_id) {

    var that = this;

    var gotMedia = function (data, status) {
      console.log('CALL SUCCESS: addTag', status);

      that.loadFiles();
    };

    var errorCallback = function () {
      console.log(Drupal.t('Error getting media.'));
    };

    $.ajax({
      url: this.settings.addTagUrl,
      type: 'GET',
      dataType: 'json',
      data: {'term_id' : term_id, 'file_id': tag_id },
      error: errorCallback,
      success: gotMedia
    });
  }

  Drupal.media.organizer.prototype.removeTag = function (term_id, tag_id) {
    var that = this;

    var gotMedia = function (data, status) {


      console.log('CALL SUCCESS', data, status);
      that.loadFiles();
    };

    var errorCallback = function () {
      console.log(Drupal.t('Error getting media.'));
    };

    $.ajax({
      url: that.settings.removeTagUrl,
      type: 'GET',
      dataType: 'json',
      data: {'term_id' : term_id, 'file_id': tag_id },
      error: errorCallback,
      success: gotMedia
    });
  }

  Drupal.media.organizer.prototype.render = function (renderElement) {
    var that = this;

    if (this.mediaFiles.length < 1) {
      $('<div id="media-empty-message" class="media-empty-message"></div>').appendTo(renderElement)
        .html(this.emptyMessage);
      return;
    }
    else {
      var mediaList = $('ul.media-list-thumbnails', renderElement);
      // If the list doesn't exist, bail.
      if (mediaList.length === 0) {
        throw('Cannot continue, list element is missing');
      }

      mediaList.html('');
    }

    while (this.cursor < this.mediaFiles.length) {
      var mediaFile = this.getNextMedia();

      var data = {};
      data.obj = this;
      data.file = mediaFile;



      var listItem = $('<li></li>').appendTo(mediaList)
//        .attr('id', 'media-item-' + mediaFile.fid)
        .html(mediaFile.preview)
//        .bind('click', data, this.clickFunction);
    }

    that.filter.position = that.cursor;

    // Reinitialize dom. We got new elements
    that.refreshDom();
  }

  Drupal.media.organizer.prototype.getNextMedia = function () {
    if (this.cursor >= this.mediaFiles.length) {
      return false;
    }
    var ret = this.mediaFiles[this.cursor];
    this.cursor += 1;
    return ret;
  };


  /**
   * Scroll handling
   *
   * @param e
   */
  Drupal.media.organizer.prototype.scrollUpdater = function (e){
    if (!e.data.loading) {
      var scrollbox = e.currentTarget;
      var scrolltop = scrollbox.scrollTop;
      var scrollheight = scrollbox.scrollHeight;
      var windowheight = scrollbox.clientHeight;
      var scrolloffset = 20;

      if(scrolltop >= (scrollheight - (windowheight + scrolloffset))) {

        // Set a flag so we don't make multiple concurrent AJAX calls
        e.data.loading = true;

        // Fetch new items
        e.data.loadFiles();
      }
    }
  };


}(jQuery));