(function ($) {
  namespace('Drupal.media.browser');

  Drupal.behaviors.organizer = {
    attach: function (context, settings) {

     var settings = Drupal.settings.media.organizer;

      $.extend(settings, Drupal.settings.media.browser.library);

      // Load media.browser library
      var browser = new Drupal.media.browser.library(settings);

      // Load media.organizer library
      var organizer = new Drupal.media.organizer(Drupal.settings.media.organizer);

      var params = {
        limit: 9
      };

      var renderElement = $('#media-organizer');


      browser.start(renderElement, params);

      $('#scrollbox').bind('scroll', browser, browser.scrollUpdater);


      // Init static concent
//      library.initDom();

      // Init dynamic content
//      library.refreshDom();

      // Initially load thumbnails
//      library.loadFiles();

      // Init dynamic content
//      library.refreshDom();

    }
  };

  /**
   * Library constructer
   * - Set defaults
   *
   * @param settings
   */
  Drupal.media.organizer = function (settings) {
    this.renderElement = $('.media-list-thumbnails');
    this.done = false; // Keeps track of if the last request for media returned 0 results.
    this.loading = false;
//    console.log('Set settings:',  this.settings);
  }

  /**
   * Init dom and attach event listener
   * - adding droppable event to tag area
   * - adding click event to tag filter
   */
  Drupal.media.organizer.prototype.initDom = function () {
    var that = this;

    // Set selectors
    var droppable_selector = $('#media-organizer .term-drop-area');

    // Init droppable event
    droppable_selector.droppable({
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      drop: function( event, ui ) {
//        console.log('Dropped item', term_id, file_id);

        // Get attributes
        var drop_container = $( this );
        var drag_element =  $(ui.draggable[0]);
        var term_id = drop_container.attr('data-term-id');
        var file_id = drag_element.attr('data-fid');

        that.addTag(term_id, file_id);
      }
    });

    // Add filter events to tags
    droppable_selector.click(function (e){

      // Toggle active class on clicked
      $(this).toggleClass('ui-state-active');

      // TODO: Speedup selector
      var attr_list = [];
      $('#media-organizer .term-drop-area.ui-state-active').each(function() {
        attr_list.push($(this).attr('data-term-id'));
      });

//      // Set global filter
      that.filter.tag_filter = attr_list.join(',');


      that.loadFiles();
    });
  }

  /**
   * Set event listener to new objects in dom
   * - draggable event
   * - remove tag event
   */
  Drupal.media.organizer.prototype.refreshDom = function () {
    var that = this;

    // Set selectors
    var draggable_selector = $('#media-organizer .media-thumbnail .img-wrapper');
    var delete_tag_selector = $('#media-organizer .delete-action');

    console.log('Media organizer: Refresh dom ...');

    // Attach remove tag event
    delete_tag_selector.click(function (e, s) {
      event.preventDefault();

      var action_link = $(this);
      var term_id = action_link.attr('data-tid');
      var file_id = action_link.attr('data-fid');

      that.removeTag(term_id, file_id);
    });

    // Init draggable event
    draggable_selector.draggable(
      {
        appendTo: 'body',
//        revert : true,
        helper: 'clone'
      });
  }

  Drupal.media.organizer.prototype.addTag = function (term_id, tag_id) {

    var that = this;

    var gotMedia = function (data, status) {
      console.log('CALL SUCCESS: addTag', status);

      that.loadFiles();
    };

    var errorCallback = function () {
      console.log(Drupal.t('Error getting media.'));
    };

    $.ajax({
      url: this.settings.addTagUrl,
      type: 'GET',
      dataType: 'json',
      data: {'term_id' : term_id, 'file_id': tag_id },
      error: errorCallback,
      success: gotMedia
    });
  }

  Drupal.media.organizer.prototype.removeTag = function (term_id, tag_id) {
    var that = this;

    var gotMedia = function (data, status) {


      console.log('CALL SUCCESS', data, status);
      that.loadFiles();
    };

    var errorCallback = function () {
      console.log(Drupal.t('Error getting media.'));
    };

    $.ajax({
      url: that.settings.removeTagUrl,
      type: 'GET',
      dataType: 'json',
      data: {'term_id' : term_id, 'file_id': tag_id },
      error: errorCallback,
      success: gotMedia
    });
  }

}(jQuery));